class Rate < ActiveRecord::Base
  URL = "http://www.wego.co.id/hotel/carebear/exchange-rates.json"

  def self.get_update
    require 'open-uri'
    new_rates = JSON.parse open(URL).read

    all_rates = self.all
    if all_rates.blank?
      self.create(new_rates['rates'].map{|key, value| {currency: key, value: value}})
    else
      new_rates['rates'].each do |key, value|
        rate_rec = all_rates.where(["currency = ?", key]).first
        unless rate_rec.blank?
          rate_rec.update_attributes({value: value})
        else
          self.create({currency: key, value: value})
        end
      end
    end

    return self.all
  end

  def self.proces_calculating(currency_from_id, currency_to_id, amount)
    return 0 if amount.to_f == 0.0

    rates = self.where("id in (#{currency_from_id}, #{currency_to_id})")
    currency_from = rates.where("id = #{currency_from_id}").first
    currency_to = rates.where("id = #{currency_to_id}").first

    return amount.to_f * (currency_to.value.to_f/currency_from.value.to_f)
  end
end
