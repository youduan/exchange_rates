class ExchangeController < ApplicationController
  before_filter :automatic_update_rate, only: [:index, :update_rate, :calculate_with_ajax, :calculate]
  before_filter :prepare_rate_data, only: [:index, :update_rate, :calculate_with_ajax]

  def index
  end

  def update_rate
  end

  def calculate_with_ajax
  end

  def calculate
    result = Rate.proces_calculating(params[:currency_from_id], params[:currency_to_id], params[:amount])

    respond_to do |format|
      format.json { render json: {result: result}}
    end
  end

  def start_update
    @all_new_rates = Rate.get_update

    respond_to do |format|
      format.js
    end
  end

  protected
  def prepare_rate_data
    @rates = Rate.all
  end

  def automatic_update_rate
    last_update = Rate.last.try(:updated_at)
    if last_update.blank? || last_update + 1.days < Time.zone.now
      Rate.get_update
    end
  end
end
