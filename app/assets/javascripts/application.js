// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

var rates = "";
$( document ).ready(function(){
  $("#start_update").click(function(){
    $.ajax({
      url: "/exchange/start_update",
      type: 'get',
      dataType: 'script',
      beforeSend: function(){
        $(this).attr("disabled", true);
        $("table tbody").empty();
        $("#status_update").html("Loading...");
      },
      complete: function(xhr, status){
        $(this).attr("disabled", false);
        $("#status_update").html("Done");
      }
    });
    return false;
  })

  $("#calcuation_with_js").click(function(){
    currency_from = $("#currency_from").val()
    currency_to = $("#currency_to").val()
    amount = $("#amount").val();

    currency_from_rec = $.grep(rates, function(e){ return e.id == currency_from; });
    currency_to_rec = $.grep(rates, function(e){ return e.id == currency_to; });

    result = amount * (currency_to_rec[0].value /currency_from_rec[0].value)

    $("#result").html(result)

    return false;
  })

  $("#calcuation_with_ajax").click(function(){
    $.ajax({
      url: "/exchange/calculate?currency_from_id="+$("#currency_from").val()+"&currency_to_id="+$("#currency_to").val()+"&amount="+$("#amount").val(),
      type: 'get',
      dataType: 'json',
      async: false,
      beforeSend: function(){
        $(this).attr("disabled", true);
        $("#result").html("Calculating...");
      },
      success: function(data){
        $("#result").html(data.result);
      }

    });

    return false;
  })

});