require 'rails_helper'

RSpec.describe ExchangeController, :type => :controller do
  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("index")
    end

    it "returns @rates" do
      get 'index'
      expect(assigns(:rates)).should_not nil
    end
  end

  describe "GET 'update_rate'" do
    it "returns http success" do
      get 'update_rate'
      response.should be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("update_rate")
    end

    it "returns @rates" do
      get 'update_rate'
      expect(assigns(:rates)).should_not nil
    end
  end

  describe "GET 'calculate_with_ajax'" do
    it "returns http success" do
      get 'calculate_with_ajax'
      response.should be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("calculate_with_ajax")
    end

    it "returns @rates" do
      get 'calculate_with_ajax'
      expect(assigns(:rates)).should_not nil
    end
  end

  describe "GET 'calculate'" do
    it "returns http success" do
      Rate.get_update
      idr = Rate.where("currency = 'IDR'").first
      usd = Rate.where("currency = 'USD'").first
      amount = 20.0

      xhr :get, 'calculate', format: :json, currency_from_id: usd.id, currency_to_id: idr.id, amount: amount
      response.should be_success
      expect(response).to have_http_status(200)
    end
  end

  describe "GET 'start_update'" do
    it "returns http success" do
      xhr :get, 'start_update', format: :js
      response.should be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("exchange/start_update")
    end

    it "returns @all_new_rates" do
      xhr :get, 'start_update', format: :js
      expect(assigns(:all_new_rates)).should_not nil
    end
  end
end