require 'rails_helper'

RSpec.describe Rate, :type => :model do
  
  context 'get_update' do
    def given_blank_db
      Rate.all.each{|s| s.destroy}
    end

    it "should get rate data from 'http://www.wego.co.id/hotel/carebear/exchange-rates.json'" do 
      given_blank_db
      rate = Rate.all
      rate.length.should == 0

      Rate.get_update
      rate = Rate.all
      rate.length.should_not == 0      
    end

    it "should get and update rate data from 'http://www.wego.co.id/hotel/carebear/exchange-rates.json'" do
      Rate.get_update
      rate_old = Rate.all
      rate_old.length.should_not == 0

      sleep(60)
      
      Rate.get_update
      rate = Rate.all
      rate.length.should_not == 0      
      rate_old.first.updated_at.should_not == rate.first.updated_at
    end
  end

  context 'proces_calculating' do
    def given_rate_data_into_db
      Rate.get_update
      idr = Rate.where("currency = 'IDR'").first
      idr.value = "12250"
      idr.save
      # usd = Rate.where("currency = 'USD'").first
    end

    it "should get result of calculation rate with some amount" do 
      given_rate_data_into_db
      idr = Rate.where("currency = 'IDR'").first
      usd = Rate.where("currency = 'USD'").first
     
      amount = 20.0

      #USD to IDR
      result = Rate.proces_calculating(usd.id, idr.id, amount)
      result.to_f.should == 245000.0

      #back -> IDR to USD
      result = Rate.proces_calculating(idr.id, usd.id, result)
      result.to_f.should == amount     
    end

    it "should get result of calculation rate with 0 amount and '' amount" do 
      given_rate_data_into_db
      idr = Rate.where("currency = 'IDR'").first
      usd = Rate.where("currency = 'USD'").first
     
      result = Rate.proces_calculating(usd.id, idr.id, 0)
      result.to_f.should == 

      result = Rate.proces_calculating(usd.id, idr.id, '')
      result.to_f.should == 0     
    end
  end
end
