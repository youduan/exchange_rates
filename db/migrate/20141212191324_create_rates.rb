class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.string :currency, :limit => 3
      t.decimal :value, :precision => 15, :scale => 6

      t.timestamps
    end
  end
end
